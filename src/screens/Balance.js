import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton, Box, Container, Grid, Typography, Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import VisibilityIcon from '@material-ui/icons/Visibility';
import TelegramIcon from '@material-ui/icons/Telegram';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import HistoryIcon from '@material-ui/icons/History';
import AddIcon from '@material-ui/icons/Add';

const transactions = [
    {
        balance: false,
        bank: 'Banesco',
        quantity: '10.200',
        bs: '1.200.000'
    },
    {
        balance: false,
        bank: 'Mercantil',
        quantity: '20.000',
        bs: '20.00.000'
    },
    {
        balance: false,
        bank: 'Provincial',
        quantity: '7.000',
        bs: '70.00.000'
    },
    {
        balance: true,
        quantity: '50.000'
    },
    {
        balance: false,
        bank: 'Bancaribe',
        quantity: '9.000',
        bs: '70.00.000'
    },
    {
        balance: true,
        quantity: '90.000'
    },
    {
        balance: true,
        quantity: '10.000'
    },
    {
        balance: false,
        bank: 'Banco Fondo Comun',
        quantity: '5.000',
        bs: '50.00.000'
    }
];

const useStyles = makeStyles({
  container: {
      flex: '3',
      flexDirection: 'column',
      height: '100vh',
      flexWrap: 'nowrap',
      backgroundColor: '#003f62',
  },
  header: {
      flex: '1',
      padding: '15px',
      color: '#fff',
  },
  content: {
      flex: '2',
      padding: '15px',
      backgroundColor: '#fff',
      borderRadius: '5px 0 0 0',
  },
  tasa: {
      position: 'relative',
      backgroundColor: '#fff',
      borderRadius: '25px',
      color: '#ff7c46',
      padding: '2px',
      paddingInline: '40px',
  },
  visibleButton: {
    position: 'absolute',
    top: '-10px',
    right: '-35px',
    color: '#fff',
    opacity: 0.5,
  },
  tasaVisibleButton: {
    position: 'absolute',
    top: '-6px',
    right: '7px',
    color: '#ff7c46',
    opacity: 0.5,
  },
  actions: {
    backgroundColor: '#ff7c46',
    width: '60px',
    height: '60px',
    borderRadius: '50%',
    marginInline: '8px',
    border: '1px solid black',
  },
  divider: {
    width: '100%',
    height: '2px',
    border: 0,
    borderTop: '2px solid #c9cace',
  },
  list: {
      width: 250
  }
});

function Balance() {
  const classes = useStyles();
  const [openDrawer, setOpenDrawer] = useState(false);

  const menuList = () => (
    <div
      className={classes.list}
      role="presentation"
      onClick={() => setOpenDrawer(false)}
      onKeyDown={() => setOpenDrawer(false)}
    >
      <List>
        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <AddIcon /> : <TelegramIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div style={{backgroundColor: 'lightgrey' }}>
        <Container style={{ maxWidth: "450px" }}>
            <Grid container className={classes.container}>
                <Grid container className={classes.header}>   
                    <Grid container style={{ justifyContent: 'space-between' }}>
                        <IconButton aria-label="menu" style={{ color: '#fff' }} onClick={() => setOpenDrawer(true)}>
                            <MenuIcon />
                        </IconButton>
                        <Drawer anchor="left" open={openDrawer} onClose={() => setOpenDrawer(false)}>
                            {menuList()}
                        </Drawer>
                        <IconButton aria-label="notificacations" style={{ color: '#fff' }}>
                            <NotificationsIcon />
                        </IconButton>
                    </Grid>

                    <Grid container style={{ alignItems: 'center', flexDirection: 'column' }}>
                        <Typography>
                            Saldo
                        </Typography>
                        <span style={{ position: 'relative' }}>
                            <Typography variant="h3">
                                <b>$100.000</b>
                            </Typography>
                            <IconButton className={classes.visibleButton}>
                                <VisibilityIcon style={{ fontSize: '20px' }} />
                            </IconButton>
                        </span>
                        <span className={classes.tasa}>
                            <Typography style={{ fontWeight: 800 }}>
                                <span style={{ fontWeight: 500 }}>Tasa:</span> 0.00135
                            </Typography>
                            <IconButton className={classes.tasaVisibleButton}>
                                <VisibilityIcon style={{ fontSize: "15px" }} />
                            </IconButton>
                        </span>
                    </Grid>

                    <Grid container style={{ justifyContent: 'center', marginTop: '15px' }}>
                        <Box className={classes.actions} boxShadow={3}>
                            <IconButton aria-label="send" style={{ color: "#fff" }}>
                                <TelegramIcon fontSize="large" />
                            </IconButton>
                        </Box>
                        <Box className={classes.actions} boxShadow={3}>
                            <IconButton aria-label="account" style={{ color: "#fff" }}>
                                <AccountBalanceWalletIcon fontSize="large" />
                            </IconButton>
                        </Box>
                        <Box className={classes.actions} boxShadow={3}>
                            <IconButton aria-label="history" style={{ color: "#fff" }}>
                                <HistoryIcon fontSize="large" />
                            </IconButton>
                        </Box>
                        <Box className={classes.actions} boxShadow={3}>
                            <IconButton aria-label="add" style={{ color: "#fff" }}>
                                <AddIcon fontSize="large" />
                            </IconButton>
                        </Box>
                    </Grid>
                </Grid>

                <Grid container className={classes.content}>
                    <Grid container style={{ justifyContent: 'space-between' }}>
                        <Typography style={{ fontSize: '18px', fontWeight: 800 }}>
                            Últimas transacciones
                        </Typography>
                        <Typography style={{ fontSize: '18px', color: '#a2e0f7', fontWeight: 600 }}>
                            Ver más
                        </Typography>
                    </Grid>

                    <hr className={classes.divider} />

                    {transactions.map(transaction => (
                        <Grid container style={{ justifyContent: 'space-between', marginTop: '8px' }}>
                            <div>
                                <Typography variant='subtitle2' style={{color: '#464646', fontWeight: 600}}>
                                    {transaction.balance ? 'Recarga de saldo' : 'Transferencia'}
                                </Typography>
                                {!transaction.balance && (
                                    <Typography variant='body2' style={{ fontWeight: '100' }}>
                                        {transaction.bank}
                                    </Typography>
                                )}
                            </div>
                            <div>
                                <Typography variant='subtitle2' align="right" style={transaction.balance ? { color: '#214555' } : { color: '#ff7c46' } }>
                                    <b>{transaction.balance ? '+' : '-'} ${transaction.quantity}</b>
                                </Typography>
                                {!transaction.balance && (
                                    <Typography variant='body2' align="right" style={{ fontWeight: '400', color: '#c9cace' }}>
                                        {transaction.bs} bs
                                    </Typography>
                                )}
                            </div>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
        </Container>
    </div>
  );
}

export default Balance;
